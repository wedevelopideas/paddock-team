<?php

namespace Tests;

use App\paddock\Users\Models\Users;
use App\paddock\Tracks\Models\Tracks;
use App\paddock\Seasons\Model\Seasons;
use App\paddock\Drivers\Models\Drivers;
use App\paddock\Countries\Models\Countries;
use App\paddock\Seasons\Model\SeasonsRaces;
use App\paddock\GrandPrixs\Model\GrandPrixs;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, DatabaseMigrations;

    /**
     * @var Countries
     */
    protected $country;

    /**
     * @var Drivers
     */
    protected $driver;

    /**
     * @var GrandPrixs
     */
    protected $grandprix;

    /**
     * @var Seasons
     */
    protected $season;

    /**
     * @var SeasonsRaces
     */
    protected $seasonRace;

    /**
     * @var Tracks
     */
    protected $track;

    /**
     * @var Users
     */
    protected $user;

    /**
     * Set up the test.
     */
    public function setUp()
    {
        parent::setUp();

        $this->country = factory(Countries::class)->create();
        $this->driver = factory(Drivers::class)->create();
        $this->grandprix = factory(GrandPrixs::class)->create();
        $this->season = factory(Seasons::class)->create();
        $this->seasonRace = factory(SeasonsRaces::class)->create();
        $this->track = factory(Tracks::class)->create();
        $this->user = factory(Users::class)->create();
    }

    /**
     * Reset the migrations.
     */
    public function tearDown()
    {
        $this->artisan('migrate:reset');
        parent::tearDown();
    }
}
