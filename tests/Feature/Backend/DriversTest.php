<?php

namespace Tests\Feature\Backend;

use Tests\TestCase;

class DriversTest extends TestCase
{
    /** @test */
    public function test_shows_the_drivers_index()
    {
        $response = $this->actingAs($this->user)->get(route('backend.drivers'));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_shows_the_drivers_form_for_creating()
    {
        $response = $this->actingAs($this->user)->get(route('backend.drivers.create'));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_shows_the_drivers_create_process()
    {
        $data = [
            'country_id' => 'de',
            'status' => 1,
            'name' => 'Sebastian Vettel',
            'slug' => 'sebastian-vettel',
            'first_name' => 'Sebastian',
            'last_name' => 'Vettel',
            'hashtag' => '#Seb5',
            'dateofbirth' => '1987-07-03',
            'dateofdeath' => '',
        ];

        $response = $this->actingAs($this->user)->post(route('backend.drivers.create'), $data);
        $response->assertStatus(302);
        $response->assertRedirect(route('backend.drivers'));
    }

    /** @test */
    public function test_shows_the_drivers_form_for_editing()
    {
        $response = $this->actingAs($this->user)->get(route('backend.drivers.edit', ['id' => $this->driver->id]));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_shows_the_drivers_update_process()
    {
        $data = [
            'id' => 1,
            'country_id' => 'de',
            'status' => 1,
            'name' => 'Sebastian Vettel',
            'slug' => 'sebastian-vettel',
            'first_name' => 'Sebastian',
            'last_name' => 'Vettel',
            'hashtag' => '#Seb5',
            'dateofbirth' => '1987-07-03',
            'dateofdeath' => '',
        ];

        $response = $this->actingAs($this->user)->post(route('backend.drivers.edit', ['id' => $data['id']]), $data);
        $response->assertStatus(302);
        $response->assertRedirect(route('backend.drivers'));
    }
}
