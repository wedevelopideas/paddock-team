<?php

namespace Tests\Feature\Backend;

use Tests\TestCase;

class GrandPrixsTest extends TestCase
{
    /** @test */
    public function test_shows_the_grandprixs_index()
    {
        $response = $this->actingAs($this->user)->get(route('backend.grandprixs'));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_shows_the_grandprixs_form_for_creating()
    {
        $response = $this->actingAs($this->user)->get(route('backend.grandprixs.create'));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_shows_the_grandprixs_create_process()
    {
        $data = [
            'country_id' => 'it',
            'name' => 'Italy',
            'slug' => 'italy',
            'full_name' => 'Italian Grand Prix',
            'hashtag' => 'ItalianGP',
            'emoji' => '🇮🇹',
            'active' => true,
        ];

        $response = $this->actingAs($this->user)->post(route('backend.grandprixs.create'), $data);
        $response->assertStatus(302);
        $response->assertRedirect(route('backend.grandprixs'));
    }

    /** @test */
    public function test_shows_the_grandprixs_form_for_editing()
    {
        $response = $this->actingAs($this->user)->get(route('backend.grandprixs.edit', ['id' => $this->grandprix->id]));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_shows_the_grandprixs_update_process()
    {
        $data = [
            'id' => 1,
            'country_id' => 'it',
            'name' => 'Italy',
            'slug' => 'italy',
            'full_name' => 'Italian Grand Prix',
            'hashtag' => 'ItalianGP',
            'emoji' => '🇮🇹',
            'active' => true,
        ];

        $response = $this->actingAs($this->user)->post(route('backend.grandprixs.edit', ['id' => $data['id']]), $data);
        $response->assertStatus(302);
        $response->assertRedirect(route('backend.grandprixs'));
    }
}
