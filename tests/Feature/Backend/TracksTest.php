<?php

namespace Tests\Feature\Backend;

use Tests\TestCase;

class TracksTest extends TestCase
{
    /** @test */
    public function test_shows_the_tracks_index()
    {
        $response = $this->actingAs($this->user)->get(route('backend.tracks'));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_shows_the_tracks_form_for_creating()
    {
        $response = $this->actingAs($this->user)->get(route('backend.tracks.create'));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_shows_the_tracks_create_process()
    {
        $data = [
            'country_id' => 'it',
            'name' => 'Autodromo Nazionale Monza',
            'slug' => 'autodromo-nazionale-monza',
            'timezone' => 'Europe/Rome',
            'active' => true,
        ];

        $response = $this->actingAs($this->user)->post(route('backend.tracks.create'), $data);
        $response->assertStatus(302);
        $response->assertRedirect(route('backend.tracks'));
    }

    /** @test */
    public function test_shows_the_tracks_form_for_editing()
    {
        $response = $this->actingAs($this->user)->get(route('backend.tracks.edit', ['id' => $this->track->id]));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_shows_the_tracks_update_process()
    {
        $data = [
            'id' => 1,
            'country_id' => 'it',
            'name' => 'Autodromo Nazionale Monza',
            'slug' => 'autodromo-nazionale-monza',
            'timezone' => 'Europe/Rome',
            'active' => true,
        ];

        $response = $this->actingAs($this->user)->post(route('backend.tracks.edit', ['id' => $data['id']]), $data);
        $response->assertStatus(302);
        $response->assertRedirect(route('backend.tracks'));
    }
}
