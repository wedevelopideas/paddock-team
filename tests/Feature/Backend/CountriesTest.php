<?php

namespace Tests\Feature\Backend;

use Tests\TestCase;

class CountriesTest extends TestCase
{
    /** @test */
    public function test_shows_the_countries_index()
    {
        $response = $this->actingAs($this->user)->get(route('backend.countries'));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_shows_the_countries_form_for_creating()
    {
        $response = $this->actingAs($this->user)->get(route('backend.countries.create'));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_shows_the_countries_create_process()
    {
        $data = [
            'code' => 'it',
            'name' => 'Italy',
        ];

        $response = $this->actingAs($this->user)->post(route('backend.countries.create'), $data);
        $response->assertStatus(302);
        $response->assertRedirect(route('backend.countries'));
    }

    /** @test */
    public function test_shows_the_countries_form_for_editing()
    {
        $response = $this->actingAs($this->user)->get(route('backend.countries.edit', ['code' => $this->country->code]));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_shows_the_countries_update_process()
    {
        $data = [
            'code' => 'it',
            'name' => 'Italy',
        ];

        $response = $this->actingAs($this->user)->post(route('backend.countries.edit', ['code' => $data['code']]), $data);
        $response->assertStatus(302);
        $response->assertRedirect(route('backend.countries'));
    }
}
