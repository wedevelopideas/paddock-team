<?php

namespace Tests\Feature\Backend;

use Tests\TestCase;

class UsersTest extends TestCase
{
    /** @test */
    public function test_shows_the_users_index()
    {
        $response = $this->actingAs($this->user)->get(route('backend.users'));
        $response->assertSuccessful();
    }
}
