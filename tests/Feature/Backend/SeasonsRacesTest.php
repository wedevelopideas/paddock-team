<?php

namespace Tests\Feature\Backend;

use Tests\TestCase;

class SeasonsRacesTest extends TestCase
{
    /** @test */
    public function test_shows_the_races_of_the_season()
    {
        $response = $this->actingAs($this->user)->get(route('backend.seasons.races', ['season' => $this->seasonRace->season]));
        $response->assertSuccessful();
    }
}
