<?php

namespace Tests\Feature\Backend;

use Tests\TestCase;

class SeasonsTest extends TestCase
{
    /** @test */
    public function test_shows_the_seasons_index()
    {
        $response = $this->actingAs($this->user)->get(route('backend.seasons'));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_shows_the_seasons_form_for_creating()
    {
        $response = $this->actingAs($this->user)->get(route('backend.seasons.create'));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_shows_the_seasons_create_process()
    {
        $data = [
            'season' => date('Y'),
        ];

        $response = $this->actingAs($this->user)->post(route('backend.seasons.create'), $data);
        $response->assertStatus(302);
        $response->assertRedirect(route('backend.seasons'));
    }

    /** @test */
    public function test_shows_the_seasons_form_for_editing()
    {
        $response = $this->actingAs($this->user)->get(route('backend.seasons.edit', ['code' => $this->season->season]));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_shows_the_seasons_update_process()
    {
        $data = [
            'season' => date('Y'),
        ];

        $response = $this->actingAs($this->user)->post(route('backend.seasons.edit', ['season' => $data['season']]), $data);
        $response->assertStatus(302);
        $response->assertRedirect(route('backend.seasons'));
    }
}
