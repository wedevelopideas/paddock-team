<?php

namespace Tests\Feature;

use Tests\TestCase;

class SettingsTest extends TestCase
{
    /** @test */
    public function test_show_the_settings()
    {
        $response = $this->actingAs($this->user)->get(route('settings'));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_update_the_user()
    {
        $data = [
            'first_name' => $this->user->first_name,
            'last_name' => $this->user->last_name,
        ];

        $response = $this->actingAs($this->user)->post(route('settings.updateUser'), $data);
        $response->assertStatus(302);
        $response->assertRedirect(route('settings'));
    }
}
