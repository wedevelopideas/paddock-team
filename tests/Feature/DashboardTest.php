<?php

namespace Tests\Feature;

use Tests\TestCase;

class DashboardTest extends TestCase
{
    /** @test */
    public function test_show_the_user_dashboard()
    {
        $response = $this->actingAs($this->user)->get(route('dashboard'));
        $response->assertSuccessful();
    }
}
