@if (session()->has('success'))
<div class="ui success message">
    <div class="header">
        {{ session()->get('success') }}
    </div>
</div>
@endif