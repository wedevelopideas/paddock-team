@extends('layouts.backend')
@section('title', 'paddock Team - Administration')
@section('content')
    <div class="ui main container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <div class="ui red segment">
                        <h1 class="ui header">
                            <i class="world icon"></i>
                            <span class="content">
                                {{ trans('common.tracks') }}
                            </span>
                        </h1>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <a href="{{ route('backend.tracks.create') }}" class="ui red right floated icon labeled button">
                        <i class="add icon"></i>
                        {{ trans('common.create_track') }}
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <table class="ui red unstackable table">
                        <thead>
                        <tr>
                            <th>{{ trans('common.name') }}</th>
                            <th>{{ trans('common.actions') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($tracks as $track)
                            <tr>
                                <td{{ ($track->active === 0) ? ' class=disabled' : '' }}><i class="{{ $track->country_id }} flag"></i> {{ $track->name }}</td>
                                <td>
                                    <a href="{{ route('backend.tracks.edit', ['id' => $track->id]) }}" class="ui red icon button">
                                        <i class="edit icon"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection