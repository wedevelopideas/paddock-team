@extends('layouts.backend')
@section('title', 'paddock Team - Administration')
@section('content')
    <div class="ui main container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <div class="ui red segment">
                        <h1 class="ui header">
                            <i class="world icon"></i>
                            <span class="content">
                                {{ trans('common.edit_track') }}
                            </span>
                        </h1>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <form action="{{ route('backend.tracks.edit', ['id' => $track->id]) }}" method="post" class="ui form">
                        @csrf

                        <div class="three fields">
                            <div class="field">
                                <label for="country_id">{{ trans('common.country') }}</label>
                                <select name="country_id" id="country_id">
                                    @foreach($countries as $country)
                                        <option value="{{ $country->code }}"{{ ($country->code == $track->country_id) ? ' selected' : '' }}>{{ $country->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="field">
                                <label for="name">{{ trans('common.name') }}</label>
                                <input type="text" name="name" placeholder="{{ trans('common.name') }}" value="{{ $track->name }}">
                            </div>
                            <div class="field">
                                <label for="timezone">{{ trans('common.timezone') }}</label>
                                <input type="text" name="timezone" placeholder="{{ trans('common.timezone') }}" value="{{ $track->timezone }}">
                            </div>
                        </div>
                        <div class="inline field">
                            <div class="ui checkbox">
                                <input type="checkbox" name="active" id="active" tabindex="0" {{ ($track->active == true) ? 'checked="checked"' : '' }}
                                       value="{{ $track->active }}" class="hidden">
                                <label for="active">{{ trans('common.active') }}</label>
                            </div>
                        </div>
                        <button class="ui red icon labeled button">
                            <i class="save icon"></i>
                            {{ trans('common.save') }}
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection