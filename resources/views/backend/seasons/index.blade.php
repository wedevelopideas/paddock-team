@extends('layouts.backend')
@section('title', 'paddock Team - Administration')
@section('content')
    <div class="ui main container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <div class="ui red segment">
                        <h1 class="ui header">
                            <i class="world icon"></i>
                            <span class="content">
                                {{ trans('common.seasons') }}
                            </span>
                        </h1>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <a href="{{ route('backend.seasons.create') }}" class="ui red right floated icon labeled button">
                        <i class="add icon"></i>
                        {{ trans('common.create_season') }}
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <table class="ui red unstackable table">
                        <thead>
                        <tr>
                            <th>{{ trans('common.season') }}</th>
                            <th>{{ trans('common.actions') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($seasons as $season)
                            <tr>
                                <td>{{ $season->season }}</td>
                                <td>
                                    <a href="{{ route('backend.seasons.edit', ['season' => $season->season]) }}" class="ui red icon button">
                                        <i class="edit icon"></i>
                                    </a>
                                    <a href="{{ route('backend.seasons.races', ['season' => $season->season]) }}" class="ui red icon button">
                                        <i class="checkered flag icon"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection