@extends('layouts.backend')
@section('title', 'paddock Team - Administration')
@section('content')
    <div class="ui main container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <div class="ui red segment">
                        <h1 class="ui header">
                            <i class="world icon"></i>
                            <span class="content">
                                {{ trans('common.season_year', ['season' => $season]) }}
                            </span>
                        </h1>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <table class="ui red unstackable table">
                        <thead>
                        <tr>
                            <th>{{ trans('common.date') }}</th>
                            <th>{{ trans('common.grandprix') }}</th>
                            <th>{{ trans('common.track') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($races as $race)
                            <tr>
                                <td>{{ $race->raceday->format('d.m.Y') }}</td>
                                <td>{{ $race->grandprix->full_name }}</td>
                                <td>{{ $race->track->name }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection