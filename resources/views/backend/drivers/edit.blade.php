@extends('layouts.backend')
@section('title', 'paddock Team - Administration')
@section('content')
    <div class="ui main container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <div class="ui red segment">
                        <h1 class="ui header">
                            <i class="world icon"></i>
                            <span class="content">
                                {{ trans('common.edit_driver') }}
                            </span>
                        </h1>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <form action="{{ route('backend.drivers.edit', ['id' => $driver->id]) }}" method="post" class="ui form">
                        @csrf

                        <div class="two fields">
                            <div class="field">
                                <label for="country_id">{{ trans('common.country') }}</label>
                                <select name="country_id" id="country_id">
                                    @foreach($countries as $country)
                                        <option value="{{ $country->code }}"{{ ($country->code == $driver->country_id) ? ' selected' : '' }}>{{ $country->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="field">
                                <label for="status">{{ trans('common.status') }}</label>
                                <select name="status" id="status">
                                    <option value="1"{{ (1 == $driver->status) ? ' selected' : '' }}>{{ trans('common.works_driver') }}</option>
                                    <option value="2"{{ (2 == $driver->status) ? ' selected' : '' }}>{{ trans('common.test_driver') }}</option>
                                </select>
                            </div>
                        </div>
                        <div class="two fields">
                            <div class="field">
                                <label for="first_name">{{ trans('common.first_name') }}</label>
                                <input type="text" name="first_name" placeholder="{{ trans('common.first_name') }}" value="{{ $driver->first_name }}">
                            </div>
                            <div class="field">
                                <label for="last_name">{{ trans('common.last_name') }}</label>
                                <input type="text" name="last_name" placeholder="{{ trans('common.last_name') }}" value="{{ $driver->last_name }}">
                            </div>
                        </div>
                        <div class="two fields">
                            <div class="field">
                                <label for="dateofbirth">{{ trans('common.dateofbirth') }}</label>
                                <input type="date" name="dateofbirth" placeholder="{{ trans('common.dateofbirth') }}" value="{{ $driver->dateofbirth }}">
                            </div>
                            <div class="field">
                                <label for="dateofdeath">{{ trans('common.dateofdeath') }}</label>
                                <input type="date" name="dateofdeath" placeholder="{{ trans('common.dateofdeath') }}" value="{{ $driver->dateofdeath }}">
                            </div>
                        </div>
                        <div class="field">
                            <label for="hashtag">{{ trans('common.hashtag') }}</label>
                            <input type="text" name="hashtag" placeholder="{{ trans('common.hashtag') }}" value="{{ $driver->hashtag }}">
                        </div>
                        <button class="ui red icon labeled button">
                            <i class="save icon"></i>
                            {{ trans('common.save') }}
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection