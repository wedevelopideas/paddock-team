@extends('layouts.backend')
@section('title', 'paddock Team - Administration')
@section('content')
    <div class="ui main container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <div class="ui red segment">
                        <h1 class="ui header">
                            <i class="world icon"></i>
                            <span class="content">
                                {{ trans('common.edit_grandprix') }}
                            </span>
                        </h1>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <form action="{{ route('backend.grandprixs.edit', ['id' => $grandprix->id]) }}" method="post"
                          class="ui form">
                        @csrf

                        <div class="two fields">
                            <div class="field">
                                <label for="country_id">{{ trans('common.country') }}</label>
                                <select name="country_id" id="country_id">
                                    @foreach($countries as $country)
                                        <option value="{{ $country->code }}"{{ ($country->code == $grandprix->country_id) ? ' selected' : '' }}>{{ $country->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="field">
                                <label for="name">{{ trans('common.name') }}</label>
                                <input type="text" name="name" placeholder="{{ trans('common.name') }}"
                                       value="{{ $grandprix->name }}">
                            </div>
                        </div>
                        <div class="field">
                            <label for="full_name">{{ trans('common.full_name') }}</label>
                            <input type="text" name="full_name" placeholder="{{ trans('common.full_name') }}"
                                   value="{{ $grandprix->full_name }}">
                        </div>
                        <div class="two fields">
                            <div class="field">
                                <label for="hashtag">{{ trans('common.hashtag') }}</label>
                                <input type="text" name="hashtag" placeholder="{{ trans('common.hashtag') }}"
                                       value="{{ $grandprix->hashtag }}">
                            </div>
                            <div class="field">
                                <label for="emoji">{{ trans('common.emoji') }}</label>
                                <input type="text" name="emoji" placeholder="{{ trans('common.emoji') }}"
                                       value="{{ $grandprix->emoji }}">
                            </div>
                        </div>
                        <div class="inline field">
                            <div class="ui{{ ($grandprix->active === 1) ? ' checked' : '' }} checkbox">
                                <input type="checkbox" name="active" id="active" tabindex="0" {{ ($grandprix->active == true) ? 'checked="checked"' : '' }}
                                       value="{{ $grandprix->active }}" class="hidden">
                                <label for="active">{{ trans('common.active') }}</label>
                            </div>
                        </div>
                        <button class="ui red icon labeled button">
                            <i class="save icon"></i>
                            {{ trans('common.save') }}
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $('.ui.checkbox')
            .checkbox()
        ;
    </script>
@endsection