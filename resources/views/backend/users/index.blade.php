@extends('layouts.backend')
@section('title', 'paddock Team - Administration')
@section('content')
    <div class="ui main container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <div class="ui red segment">
                        <h1 class="ui header">
                            <i class="users icon"></i>
                            <span class="content">
                                {{ trans('common.users') }}
                            </span>
                        </h1>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <table class="ui red unstackable table">
                        <thead>
                        <tr>
                            <th>{{ trans('common.name') }}</th>
                            <th>{{ trans('common.actions') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td>
                                    <img src="{{ $user->avatar() }}" class="ui avatar image" alt="{{ $user->display_name }}">
                                    <span>{{ $user->display_name }}</span>
                                </td>
                                <td></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection