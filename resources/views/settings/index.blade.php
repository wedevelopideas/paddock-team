@extends('layouts.frontend')
@section('title', 'paddock Team')
@section('content')
    <div class="ui main container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <div class="ui red segment">
                        <h1 class="ui header">
                            <i class="settings icon"></i>
                            <span class="content">
                                {{ trans('common.settings') }}
                            </span>
                        </h1>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="four wide column">
                    <div class="ui red segment">
                        <div class="ui medium image">
                            <img src="{{ $user->avatar() }}" alt="{{ $user->display_name }}">
                        </div>
                    </div>
                </div>
                <div class="twelve wide column">
                    @include('_partials.notifications')
                    <div class="ui red clearing segment">
                        <form action="{{ route('settings.updateUser') }}" method="post" class="ui form">
                            @csrf

                            <div class="two fields">
                                <div class="field{{ $errors->has('first_name') ? ' error' : '' }}">
                                    <label for="first_name">{{ trans('common.first_name') }}</label>
                                    <input type="text" name="first_name" placeholder="{{ trans('common.first_name') }}" value="{{ $user->first_name }}">
                                </div>
                                <div class="field{{ $errors->has('last_name') ? ' error' : '' }}">
                                    <label for="last_name">{{ trans('common.last_name') }}</label>
                                    <input type="text" name="last_name" placeholder="{{ trans('common.last_name') }}" value="{{ $user->last_name }}">
                                </div>
                            </div>
                            <button type="submit" class="ui red right floated labeled icon button">
                                <i class="save icon"></i>
                                {{ trans('common.save') }}
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection