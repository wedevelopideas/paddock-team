<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, viewport-fit=cover">
    <title>@yield('title')</title>
    <meta name="description" content="paddock Team">
    <link rel="stylesheet" href="{{ asset('assets/semantic.min.css') }}">
    <style type="text/css">
        body {
            background-color: #FFFFFF;
        }
        .ui.menu .item img.logo {
            margin-right: 1.5em;
        }
        .main.container {
            margin-top: 7em;
            padding: 0 1em;
        }
        @yield('styles')
    </style>
</head>
<body>
    <div class="ui fixed inverted menu">
        <div class="ui fluid container">
            <a href="{{ route('backend') }}" class="header item">
                paddock Team
            </a>
            <a href="{{ route('backend.countries') }}" class="item">
                {{ trans('common.countries') }}
            </a>
            <a href="{{ route('backend.drivers') }}" class="item">
                {{ trans('common.drivers') }}
            </a>
            <a href="{{ route('backend.seasons') }}" class="item">
                {{ trans('common.seasons') }}
            </a>
            <a href="{{ route('backend.grandprixs') }}" class="item">
                {{ trans('common.grandprixs') }}
            </a>
            <a href="{{ route('backend.tracks') }}" class="item">
                {{ trans('common.tracks') }}
            </a>
            <a href="{{ route('backend.users') }}" class="item">
                {{ trans('common.users') }}
            </a>
            <div class="right menu">
                <a href="{{ route('dashboard') }}" class="icon item">
                    <i class="dashboard icon"></i>
                </a>
                <a href="{{ route('logout') }}" class="icon item">
                    <i class="sign out icon"></i>
                </a>
            </div>
        </div>
    </div>
@yield('content')
<script src="{{ asset('assets/jquery.min.js') }}"></script>
<script src="{{ asset('assets/semantic.min.js') }}"></script>
@yield('scripts')
</body>
</html>