<div class="ui red segment">
    <div class="ui stackable two column grid">
        <div class="column">
            <div class="ui medium image">
                <img src="{{ auth()->user()->avatar() }}" alt="{{ auth()->user()->display_name }}">
            </div>
        </div>
        <div class="column">
            <a href="{{ route('settings') }}" class="ui red fluid labeled icon button">
                <i class="settings icon"></i>
                {{ trans('common.settings') }}
            </a>
        </div>
    </div>
</div>