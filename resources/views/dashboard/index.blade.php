@extends('layouts.frontend')
@section('title', 'paddock Team')
@section('content')
    <div class="ui main fluid container">
        <div class="ui stackable grid">
            <div class="four wide column">
                @include('dashboard.components.user')
            </div>
        </div>
    </div>
@endsection