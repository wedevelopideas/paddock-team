<?php

namespace App\paddock\Tracks\Repositories;

use App\paddock\Tracks\Models\Tracks;

class TracksRepository
{
    /**
     * @var Tracks
     */
    private $tracks;

    /**
     * TracksRepository constructor.
     * @param Tracks $tracks
     */
    public function __construct(Tracks $tracks)
    {
        $this->tracks = $tracks;
    }

    /**
     * Get all grand prix.
     *
     * @return mixed
     */
    public function getAll()
    {
        return $this->tracks
            ->orderBy('name', 'ASC')
            ->get();
    }

    /**
     * Get grand prix by id.
     *
     * @param int $id
     * @return mixed
     */
    public function getTrackByID(int $id)
    {
        return $this->tracks
            ->where('id', $id)
            ->first();
    }

    /**
     * Store new grand prixs.
     *
     * @param array $data
     */
    public function store(array $data)
    {
        $tracks = new Tracks();
        $tracks->country_id = $data['country_id'];
        $tracks->name = $data['name'];
        $tracks->slug = $data['slug'];
        $tracks->timezone = $data['timezone'];
        $tracks->active = $data['active'];
        $tracks->save();
    }

    /**
     * Update requested track.
     *
     * @param array $data
     * @param int $id
     */
    public function update(array $data, int $id)
    {
        $tracks = Tracks::find($id);

        if ($tracks) {
            $tracks->country_id = $data['country_id'];
            $tracks->name = $data['name'];
            $tracks->slug = $data['slug'];
            $tracks->timezone = $data['timezone'];
            $tracks->active = $data['active'];
            $tracks->save();
        }
    }
}
