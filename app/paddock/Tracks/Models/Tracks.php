<?php

namespace App\paddock\Tracks\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Tracks.
 *
 * @property int $id
 * @property int $country_id
 * @property string $name
 * @property string $slug
 * @property string $timezone
 * @property bool $active
 */
class Tracks extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'country_id',
        'name',
        'slug',
        'timezone',
        'active',
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
