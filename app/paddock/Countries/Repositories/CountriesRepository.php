<?php

namespace App\paddock\Countries\Repositories;

use App\paddock\Countries\Models\Countries;

class CountriesRepository
{
    /**
     * @var Countries
     */
    private $countries;

    /**
     * CountriesRepository constructor.
     * @param Countries $countries
     */
    public function __construct(Countries $countries)
    {
        $this->countries = $countries;
    }

    /**
     * Get all countries.
     *
     * @return mixed
     */
    public function getAll()
    {
        return $this->countries
            ->orderBy('code', 'ASC')
            ->get();
    }

    /**
     * Get country by code.
     *
     * @param string $code
     * @return mixed
     */
    public function getCountryByCode(string $code)
    {
        return $this->countries
            ->where('code', $code)
            ->first();
    }

    /**
     * Store new countries.
     *
     * @param array $data
     */
    public function store(array $data)
    {
        $country = new Countries();
        $country->code = $data['code'];
        $country->name = $data['name'];
        $country->save();
    }

    /**
     * Update requested country.
     *
     * @param array $data
     * @param string $code
     */
    public function update(array $data, string $code)
    {
        $country = Countries::find($code);

        if ($country) {
            $country->code = $data['code'];
            $country->name = $data['name'];
            $country->save();
        }
    }
}
