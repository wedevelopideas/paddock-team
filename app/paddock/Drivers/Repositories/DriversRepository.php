<?php

namespace App\paddock\Drivers\Repositories;

use App\paddock\Drivers\Models\Drivers;

class DriversRepository
{
    /**
     * @var Drivers
     */
    private $drivers;

    /**
     * DriversRepository constructor.
     * @param Drivers $drivers
     */
    public function __construct(Drivers $drivers)
    {
        $this->drivers = $drivers;
    }

    /**
     * Get all countries.
     *
     * @return mixed
     */
    public function getAll()
    {
        return $this->drivers
            ->orderBy('last_name', 'ASC')
            ->orderBy('first_name', 'ASC')
            ->get();
    }

    /**
     * Get driver by id.
     *
     * @param int $id
     * @return mixed
     */
    public function getDriverByID(int $id)
    {
        return $this->drivers
            ->where('id', $id)
            ->first();
    }

    /**
     * Store new drivers.
     *
     * @param array $data
     */
    public function store(array $data)
    {
        $drivers = new Drivers();
        $drivers->country_id = $data['country_id'];
        $drivers->status = $data['status'];
        $drivers->name = $data['name'];
        $drivers->slug = $data['slug'];
        $drivers->first_name = $data['first_name'];
        $drivers->last_name = $data['last_name'];
        $drivers->hashtag = $data['hashtag'];
        $drivers->dateofbirth = $data['dateofbirth'];
        $drivers->dateofdeath = $data['dateofdeath'];
        $drivers->save();
    }

    /**
     * Update requested driver.
     *
     * @param array $data
     * @param int $id
     */
    public function update(array $data, int $id)
    {
        $drivers = Drivers::find($id);

        if ($drivers) {
            $drivers->country_id = $data['country_id'];
            $drivers->status = $data['status'];
            $drivers->name = $data['name'];
            $drivers->slug = $data['slug'];
            $drivers->first_name = $data['first_name'];
            $drivers->last_name = $data['last_name'];
            $drivers->hashtag = $data['hashtag'];
            $drivers->dateofbirth = $data['dateofbirth'];
            $drivers->dateofdeath = $data['dateofdeath'];
            $drivers->save();
        }
    }
}
