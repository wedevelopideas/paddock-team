<?php

namespace App\paddock\Drivers\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Drivers.
 *
 * @property int $id
 * @property string $country_id
 * @property int $status
 * @property string $name
 * @property string $slug
 * @property string $first_name
 * @property string $last_name
 * @property string $hashtag
 * @property date $dateofbirth
 * @property date $dateofdeath
 */
class Drivers extends Model
{
    /**
     * Status of a works driver.
     *
     * @var string;
     */
    const WORKS = 1;

    /**
     * Status of a test driver.
     *
     * @var string
     */
    const TEST = 2;

    /**
     * Status of a private driver.
     *
     * @var string
     */
    const PRIVATE = 3;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'country_id',
        'status',
        'name',
        'slug',
        'first_name',
        'last_name',
        'hashtag',
        'dateofbirth',
        'dateofdeath',
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
