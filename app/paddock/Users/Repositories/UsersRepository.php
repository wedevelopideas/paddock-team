<?php

namespace App\paddock\Users\Repositories;

use App\paddock\Users\Models\Users;

class UsersRepository
{
    /**
     * @var Users
     */
    private $users;

    /**
     * UsersRepository constructor.
     * @param Users $users
     */
    public function __construct(Users $users)
    {
        $this->users = $users;
    }

    /**
     * Get all users.
     *
     * @return mixed
     */
    public function getAll()
    {
        return $this->users
            ->orderBy('display_name', 'ASC')
            ->get();
    }

    /**
     * Get the data of the user by his id.
     *
     * @param int $id
     * @return mixed
     */
    public function getUserByID(int $id)
    {
        return $this->users
            ->where('id', $id)
            ->firstOrFail();
    }

    /**
     * Update the user.
     *
     * @param int $id
     * @param array $data
     */
    public function updateUser(int $id, array $data)
    {
        $user = Users::find($id);

        if ($user) {
            $user->first_name = $data['first_name'];
            $user->last_name = $data['last_name'];
            $user->save();
        }
    }
}
