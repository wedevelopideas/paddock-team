<?php

namespace App\paddock\Users\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * Class Users.
 *
 * @property int $id
 * @property string $email
 * @property string $password
 * @property string $first_name
 * @property string $last_name
 * @property string $display_name
 * @property string $user_name
 * @property string $lang
 * @property string $timezone
 * @property string $avatar
 * @property int $background_image
 * @property string|null $remember_token
 */
class Users extends Authenticatable
{
    /**
     * Default avatar of the user.
     *
     * @var string
     */
    const DEFAULT_AVATAR = 'https://via.placeholder.com/300'; //TODO: another default avatar

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'password',
        'first_name',
        'last_name',
        'display_name',
        'user_name',
        'lang',
        'timezone',
        'avatar',
        'background_image',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * Get avatar of the user.
     *
     * @return string
     */
    public function avatar()
    {
        return self::DEFAULT_AVATAR;
    }
}
