<?php

namespace App\paddock\Seasons\Repositories;

use App\paddock\Seasons\Model\Seasons;

class SeasonsRepository
{
    /**
     * @var Seasons
     */
    private $seasons;

    /**
     * SeasonsRepository constructor.
     * @param Seasons $seasons
     */
    public function __construct(Seasons $seasons)
    {
        $this->seasons = $seasons;
    }

    /**
     * Get all seasons.
     *
     * @return mixed
     */
    public function getAll()
    {
        return $this->seasons
            ->orderBy('season', 'ASC')
            ->get();
    }

    /**
     * Get season by season.
     *
     * @param int $season
     * @return mixed
     */
    public function getSeasonBySeason(int $season)
    {
        return $this->seasons
            ->where('season', $season)
            ->first();
    }

    /**
     * Store new seasons.
     *
     * @param array $data
     */
    public function store(array $data)
    {
        $seasons = new Seasons();
        $seasons->season = $data['season'];
        $seasons->save();
    }

    /**
     * Update requested seasons.
     *
     * @param array $data
     * @param int $season
     */
    public function update(array $data, int $season)
    {
        $seasons = Seasons::find($season);

        if ($seasons) {
            $seasons->season = $data['season'];
            $seasons->save();
        }
    }
}
