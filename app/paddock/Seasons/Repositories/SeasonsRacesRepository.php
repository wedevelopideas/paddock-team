<?php

namespace App\paddock\Seasons\Repositories;

use App\paddock\Seasons\Model\SeasonsRaces;

class SeasonsRacesRepository
{
    /**
     * @var SeasonsRaces
     */
    private $seasonsRaces;

    /**
     * SeasonsRacesRepository constructor.
     * @param SeasonsRaces $seasonsRaces
     */
    public function __construct(SeasonsRaces $seasonsRaces)
    {
        $this->seasonsRaces = $seasonsRaces;
    }

    /**
     * Get all races by season.
     *
     * @param int $season
     * @return mixed
     */
    public function getRacesBySeason(int $season)
    {
        return $this->seasonsRaces
            ->where('season', $season)
            ->get();
    }
}
