<?php

namespace App\paddock\Seasons\Model;

use App\paddock\Tracks\Models\Tracks;
use Illuminate\Database\Eloquent\Model;
use App\paddock\GrandPrixs\Model\GrandPrixs;

/**
 * Class SeasonsRaces.
 *
 * @property int $id
 * @property int $season
 * @property int $gp_id
 * @property int $track_id
 * @property \Illuminate\Support\Carbon $raceday
 * @property int $status
 * @property-read \App\paddock\GrandPrixs\Model\GrandPrixs $grandprix
 * @property-read \App\paddock\Tracks\Models\Tracks $track
 */
class SeasonsRaces extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'season',
        'gp_id',
        'track_id',
        'raceday',
        'status',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'raceday',
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Get the Grand Prix associated with the season race.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function grandprix()
    {
        return $this->belongsTo(GrandPrixs::class, 'gp_id');
    }

    /**
     * Get the track associated with the season race.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function track()
    {
        return $this->belongsTo(Tracks::class, 'track_id');
    }
}
