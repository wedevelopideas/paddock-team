<?php

namespace App\paddock\GrandPrixs\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class GrandPrixs.
 *
 * @property id $id
 * @property string $country_id
 * @property string $name
 * @property string $slug
 * @property string $full_name
 * @property string $hashtag
 * @property string $emoji
 * @property bool $active
 */
class GrandPrixs extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'country_id',
        'name',
        'slug',
        'full_name',
        'hashtag',
        'emoji',
        'active',
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
