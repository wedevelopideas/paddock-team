<?php

namespace App\paddock\GrandPrixs\Repositories;

use App\paddock\GrandPrixs\Model\GrandPrixs;

class GrandPrixsRepository
{
    /**
     * @var GrandPrixs
     */
    private $grandPrixs;

    /**
     * GrandPrixsRepository constructor.
     * @param GrandPrixs $grandPrixs
     */
    public function __construct(GrandPrixs $grandPrixs)
    {
        $this->grandPrixs = $grandPrixs;
    }

    /**
     * Get all grand prix.
     *
     * @return mixed
     */
    public function getAll()
    {
        return $this->grandPrixs
            ->orderBy('name', 'ASC')
            ->get();
    }

    /**
     * Get grand prix by id.
     *
     * @param int $id
     * @return mixed
     */
    public function getGrandPrixByID(int $id)
    {
        return $this->grandPrixs
            ->where('id', $id)
            ->first();
    }

    /**
     * Store new grand prixs.
     *
     * @param array $data
     */
    public function store(array $data)
    {
        $grandprixs = new GrandPrixs();
        $grandprixs->country_id = $data['country_id'];
        $grandprixs->name = $data['name'];
        $grandprixs->slug = $data['slug'];
        $grandprixs->full_name = $data['full_name'];
        $grandprixs->hashtag = $data['hashtag'];
        $grandprixs->emoji = $data['emoji'];
        $grandprixs->active = $data['active'];
        $grandprixs->save();
    }

    /**
     * Update requested grand prix.
     *
     * @param array $data
     * @param int $id
     */
    public function update(array $data, int $id)
    {
        $grandprixs = GrandPrixs::find($id);

        if ($grandprixs) {
            $grandprixs->country_id = $data['country_id'];
            $grandprixs->name = $data['name'];
            $grandprixs->slug = $data['slug'];
            $grandprixs->full_name = $data['full_name'];
            $grandprixs->hashtag = $data['hashtag'];
            $grandprixs->emoji = $data['emoji'];
            $grandprixs->active = $data['active'];
            $grandprixs->save();
        }
    }
}
