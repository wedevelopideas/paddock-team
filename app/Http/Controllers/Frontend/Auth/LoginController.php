<?php

namespace App\Http\Controllers\Frontend\Auth;

use App\Http\Controllers\Controller;
use App\paddock\Users\Requests\LoginRequest;

class LoginController extends Controller
{
    /**
     * LoginController constructor.
     */
    public function __construct()
    {
        $this->middleware('guest')->except('handleLogout');
    }

    /**
     * Show the login form.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showLoginForm()
    {
        return view('auth.login');
    }

    /**
     * Handles the login request.
     *
     * @param LoginRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function handleLogin(LoginRequest $request)
    {
        $details = $request->only('email', 'password');

        if (auth()->attempt($details)) {
            return redirect()
                ->route('dashboard');
        }

        return redirect()
            ->route('login');
    }

    /**
     * Handles the logout request.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function handleLogout()
    {
        auth()->logout();

        return redirect()
            ->route('index');
    }
}
