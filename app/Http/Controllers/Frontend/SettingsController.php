<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\paddock\Users\Requests\UpdateUserRequest;
use App\paddock\Users\Repositories\UsersRepository;

class SettingsController extends Controller
{
    /**
     * @var UsersRepository
     */
    private $usersRepository;

    /**
     * SettingsController constructor.
     * @param UsersRepository $usersRepository
     */
    public function __construct(UsersRepository $usersRepository)
    {
        $this->usersRepository = $usersRepository;
    }

    /**
     * Displays the user's settings.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $userID = auth()->id();
        $user = $this->usersRepository->getUserByID($userID);

        return view('settings.index')
            ->with('user', $user);
    }

    /**
     * Update the user.
     *
     * @param UpdateUserRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateUser(UpdateUserRequest $request)
    {
        $id = auth()->id();

        $data = [
            'first_name' => $request->input('first_name'),
            'last_name' => $request->input('last_name'),
        ];

        $this->usersRepository->updateUser($id, $data);

        return redirect()
            ->route('settings')
            ->with('success', trans('message.user.successful_update'));
    }
}
