<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\paddock\Drivers\Requests\EditDriversRequest;
use App\paddock\Drivers\Requests\CreateDriversRequest;
use App\paddock\Drivers\Repositories\DriversRepository;
use App\paddock\Countries\Repositories\CountriesRepository;

class DriversController extends Controller
{
    /**
     * @var CountriesRepository
     */
    private $countriesRepository;

    /**
     * @var DriversRepository
     */
    private $driversRepository;

    /**
     * DriversController constructor.
     * @param CountriesRepository $countriesRepository
     * @param DriversRepository $driversRepository
     */
    public function __construct(
        CountriesRepository $countriesRepository,
        DriversRepository $driversRepository
    ) {
        $this->countriesRepository = $countriesRepository;
        $this->driversRepository = $driversRepository;
    }

    /**
     * Shows a list of all drivers.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $drivers = $this->driversRepository->getAll();

        return view('backend.drivers.index')
            ->with('drivers', $drivers);
    }

    /**
     * Form for creating new driver.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $countries = $this->countriesRepository->getAll();

        return view('backend.drivers.create')
            ->with('countries', $countries);
    }

    /**
     * Store new drivers.
     *
     * @param CreateDriversRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateDriversRequest $request)
    {
        $first_name = $request->input('first_name');
        $last_name = $request->input('last_name');
        $name = $first_name.' '.$last_name;
        $slug = str_slug($name);

        $data = [
            'country_id' => $request->input('country_id'),
            'status' => $request->input('status'),
            'name' => $name,
            'slug' => $slug,
            'first_name' => $first_name,
            'last_name' => $last_name,
            'hashtag' => $request->input('hashtag'),
            'dateofbirth' => $request->input('dateofbirth'),
            'dateofdeath' => $request->input('dateofdeath'),
        ];

        $this->driversRepository->store($data);

        return redirect()
            ->route('backend.drivers');
    }

    /**
     * Form for editing drivers.
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id)
    {
        $driver = $this->driversRepository->getDriverByID($id);

        $countries = $this->countriesRepository->getAll();

        return view('backend.drivers.edit')
            ->with('countries', $countries)
            ->with('driver', $driver);
    }

    /**
     * Update requested country.
     *
     * @param EditDriversRequest $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(EditDriversRequest $request, int $id)
    {
        $first_name = $request->input('first_name');
        $last_name = $request->input('last_name');
        $name = $first_name.' '.$last_name;
        $slug = str_slug($name);

        $data = [
            'id' => $request->input('id'),
            'country_id' => $request->input('country_id'),
            'status' => $request->input('status'),
            'name' => $name,
            'slug' => $slug,
            'first_name' => $first_name,
            'last_name' => $last_name,
            'hashtag' => $request->input('hashtag'),
            'dateofbirth' => $request->input('dateofbirth'),
            'dateofdeath' => $request->input('dateofdeath'),
        ];

        $this->driversRepository->update($data, $id);

        return redirect()
            ->route('backend.drivers');
    }
}
