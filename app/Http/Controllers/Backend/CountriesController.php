<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\paddock\Countries\Requests\EditCountriesRequest;
use App\paddock\Countries\Requests\CreateCountriesRequest;
use App\paddock\Countries\Repositories\CountriesRepository;

class CountriesController extends Controller
{
    /**
     * @var CountriesRepository
     */
    private $countriesRepository;

    /**
     * CountriesController constructor.
     * @param CountriesRepository $countriesRepository
     */
    public function __construct(CountriesRepository $countriesRepository)
    {
        $this->countriesRepository = $countriesRepository;
    }

    /**
     * Shows a list of all countries.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $countries = $this->countriesRepository->getAll();

        return view('backend.countries.index')
            ->with('countries', $countries);
    }

    /**
     * Form for creating new countries.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('backend.countries.create');
    }

    /**
     * Store new countries.
     *
     * @param CreateCountriesRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateCountriesRequest $request)
    {
        $data = [
            'code' => $request->input('code'),
            'name' => $request->input('name'),
        ];

        $this->countriesRepository->store($data);

        return redirect()
            ->route('backend.countries');
    }

    /**
     * Form for editing countries.
     *
     * @param string $code
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(string $code)
    {
        $country = $this->countriesRepository->getCountryByCode($code);

        return view('backend.countries.edit')
            ->with('country', $country);
    }

    /**
     * Update requested country.
     *
     * @param EditCountriesRequest $request
     * @param string $code
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(EditCountriesRequest $request, string $code)
    {
        $data = [
            'code' => $request->input('code'),
            'name' => $request->input('name'),
        ];

        $this->countriesRepository->update($data, $code);

        return redirect()
            ->route('backend.countries');
    }
}
