<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\paddock\Seasons\Requests\EditSeasonsRequest;
use App\paddock\Seasons\Requests\CreateSeasonsRequest;
use App\paddock\Seasons\Repositories\SeasonsRepository;

class SeasonsController extends Controller
{
    /**
     * @var SeasonsRepository
     */
    private $seasonsRepository;

    /**
     * SeasonsController constructor.
     * @param SeasonsRepository $seasonsRepository
     */
    public function __construct(SeasonsRepository $seasonsRepository)
    {
        $this->seasonsRepository = $seasonsRepository;
    }

    /**
     * Shows a list of all seasons.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $seasons = $this->seasonsRepository->getAll();

        return view('backend.seasons.index')
            ->with('seasons', $seasons);
    }

    /**
     * Form for creating new seasons.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('backend.seasons.create');
    }

    /**
     * Store new seasons.
     *
     * @param CreateSeasonsRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateSeasonsRequest $request)
    {
        $data = [
            'season' => $request->input('season'),
        ];

        $this->seasonsRepository->store($data);

        return redirect()
            ->route('backend.seasons');
    }

    /**
     * Form for editing seasons.
     *
     * @param int $season
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $season)
    {
        $season = $this->seasonsRepository->getSeasonBySeason($season);

        return view('backend.seasons.edit')
            ->with('season', $season);
    }

    /**
     * Update requested country.
     *
     * @param EditSeasonsRequest $request
     * @param int $season
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(EditSeasonsRequest $request, int $season)
    {
        $data = [
            'season' => $request->input('season'),
        ];

        $this->seasonsRepository->update($data, $season);

        return redirect()
            ->route('backend.seasons');
    }
}
