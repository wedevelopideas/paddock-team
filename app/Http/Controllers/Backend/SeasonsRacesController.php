<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\paddock\Seasons\Repositories\SeasonsRacesRepository;

class SeasonsRacesController extends Controller
{
    /**
     * @var SeasonsRacesRepository
     */
    private $seasonsRacesRepository;

    /**
     * SeasonsRacesController constructor.
     * @param SeasonsRacesRepository $seasonsRacesRepository
     */
    public function __construct(SeasonsRacesRepository $seasonsRacesRepository)
    {
        $this->seasonsRacesRepository = $seasonsRacesRepository;
    }

    /**
     * Shows a list of all season races.
     *
     * @param int $season
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function races(int $season)
    {
        $races = $this->seasonsRacesRepository->getRacesBySeason($season);

        return view('backend.seasons.races')
            ->with('races', $races)
            ->with('season', $season);
    }
}
