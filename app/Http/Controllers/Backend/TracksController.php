<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\paddock\Tracks\Requests\EditTracksRequest;
use App\paddock\Tracks\Requests\CreateTracksRequest;
use App\paddock\Tracks\Repositories\TracksRepository;
use App\paddock\Countries\Repositories\CountriesRepository;

class TracksController extends Controller
{
    /**
     * @var CountriesRepository
     */
    private $countriesRepository;

    /**
     * @var TracksRepository
     */
    private $tracksRepository;

    /**
     * TracksController constructor.
     * @param CountriesRepository $countriesRepository
     * @param TracksRepository $tracksRepository
     */
    public function __construct(
        CountriesRepository $countriesRepository,
        TracksRepository $tracksRepository
    ) {
        $this->countriesRepository = $countriesRepository;
        $this->tracksRepository = $tracksRepository;
    }

    /**
     * Shows a list of all tracks.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $tracks = $this->tracksRepository->getAll();

        return view('backend.tracks.index')
            ->with('tracks', $tracks);
    }

    /**
     * Form for creating new tracks.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $countries = $this->countriesRepository->getAll();

        return view('backend.tracks.create')
            ->with('countries', $countries);
    }

    /**
     * Store new tracks.
     *
     * @param CreateTracksRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateTracksRequest $request)
    {
        $data = [
            'country_id' => $request->input('country_id'),
            'name' => $request->input('name'),
            'slug' => str_slug($request->input('name')),
            'timezone' => $request->input('timezone'),
            'active' => $request->has('active'),
        ];

        $this->tracksRepository->store($data);

        return redirect()
            ->route('backend.tracks');
    }

    /**
     * Form for editing tracks.
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id)
    {
        $track = $this->tracksRepository->getTrackByID($id);

        $countries = $this->countriesRepository->getAll();

        return view('backend.tracks.edit')
            ->with('countries', $countries)
            ->with('track', $track);
    }

    /**
     * Update requested track.
     *
     * @param EditTracksRequest $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(EditTracksRequest $request, int $id)
    {
        $data = [
            'country_id' => $request->input('country_id'),
            'name' => $request->input('name'),
            'slug' => str_slug($request->input('name')),
            'timezone' => $request->input('timezone'),
            'active' => $request->has('active'),
        ];

        $this->tracksRepository->update($data, $id);

        return redirect()
            ->route('backend.tracks');
    }
}
