<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\paddock\GrandPrixs\Requests\EditGrandPrixsRequest;
use App\paddock\Countries\Repositories\CountriesRepository;
use App\paddock\GrandPrixs\Requests\CreateGrandPrixsRequest;
use App\paddock\GrandPrixs\Repositories\GrandPrixsRepository;

class GrandPrixsController extends Controller
{
    /**
     * @var CountriesRepository
     */
    private $countriesRepository;

    /**
     * @var GrandPrixsRepository
     */
    private $grandPrixsRepository;

    /**
     * GrandPrixsController constructor.
     * @param CountriesRepository $countriesRepository
     * @param GrandPrixsRepository $grandPrixsRepository
     */
    public function __construct(
        CountriesRepository $countriesRepository,
        GrandPrixsRepository $grandPrixsRepository
    ) {
        $this->countriesRepository = $countriesRepository;
        $this->grandPrixsRepository = $grandPrixsRepository;
    }

    /**
     * Shows a list of all grand prix.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $grandprixs = $this->grandPrixsRepository->getAll();

        return view('backend.grandprixs.index')
            ->with('grandprixs', $grandprixs);
    }

    /**
     * Form for creating new grand prix.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $countries = $this->countriesRepository->getAll();

        return view('backend.grandprixs.create')
            ->with('countries', $countries);
    }

    /**
     * Store new grand prixs.
     *
     * @param CreateGrandPrixsRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateGrandPrixsRequest $request)
    {
        $data = [
            'country_id' => $request->input('country_id'),
            'name' => $request->input('name'),
            'slug' => str_slug($request->input('name')),
            'full_name' => $request->input('full_name'),
            'hashtag' => $request->input('hashtag'),
            'emoji' => $request->input('emoji'),
            'active' => $request->has('active'),
        ];

        $this->grandPrixsRepository->store($data);

        return redirect()
            ->route('backend.grandprixs');
    }

    /**
     * Form for editing grandprixs.
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id)
    {
        $grandprix = $this->grandPrixsRepository->getGrandPrixByID($id);

        $countries = $this->countriesRepository->getAll();

        return view('backend.grandprixs.edit')
            ->with('countries', $countries)
            ->with('grandprix', $grandprix);
    }

    /**
     * Update requested grand prix.
     *
     * @param EditGrandPrixsRequest $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(EditGrandPrixsRequest $request, int $id)
    {
        $data = [
            'country_id' => $request->input('country_id'),
            'name' => $request->input('name'),
            'slug' => str_slug($request->input('name')),
            'full_name' => $request->input('full_name'),
            'hashtag' => $request->input('hashtag'),
            'emoji' => $request->input('emoji'),
            'active' => $request->has('active'),
        ];

        $this->grandPrixsRepository->update($data, $id);

        return redirect()
            ->route('backend.grandprixs');
    }
}
