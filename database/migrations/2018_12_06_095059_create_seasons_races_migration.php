<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeasonsRacesMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seasons_races', function (Blueprint $table) {
            $table->smallIncrements('id');
            $table->unsignedSmallInteger('season');
            $table->unsignedSmallInteger('gp_id');
            $table->unsignedSmallInteger('track_id');
            $table->date('raceday');
            $table->boolean('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seasons_races');
    }
}
