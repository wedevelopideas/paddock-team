<?php

use Faker\Generator as Faker;

$factory->define(\App\paddock\Seasons\Model\SeasonsRaces::class, function (Faker $faker) {
    return [
        'season' => function () {
            return factory(\App\paddock\Seasons\Model\Seasons::class)->create()->season;
        },
        'gp_id' => function () {
            return factory(\App\paddock\GrandPrixs\Model\GrandPrixs::class)->create()->id;
        },
        'track_id' => function () {
            return factory(\App\paddock\Tracks\Models\Tracks::class)->create()->id;
        },
        'raceday' => $faker->date('Y-m-d'),
        'status' => $faker->boolean,
    ];
});
