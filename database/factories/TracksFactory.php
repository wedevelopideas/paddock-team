<?php

use Faker\Generator as Faker;

$factory->define(\App\paddock\Tracks\Models\Tracks::class, function (Faker $faker) {
    return [
        'country_id' => function () {
            return factory(\App\paddock\Countries\Models\Countries::class)->create()->code;
        },
        'name' => $faker->name,
        'slug' => $faker->slug,
        'timezone' => $faker->timezone,
        'active' => $faker->boolean,
    ];
});
