<?php

use Faker\Generator as Faker;

$factory->define(\App\paddock\GrandPrixs\Model\GrandPrixs::class, function (Faker $faker) {
    return [
        'country_id' => $faker->countryCode,
        'name' => $faker->name,
        'slug' => $faker->slug,
        'full_name' => $faker->name,
        'hashtag' => $faker->text,
        'emoji' => $faker->emoji,
        'active' => $faker->boolean,
    ];
});
