<?php

use Faker\Generator as Faker;

$factory->define(\App\paddock\Seasons\Model\Seasons::class, function (Faker $faker) {
    return [
        'season' => $faker->year,
    ];
});
