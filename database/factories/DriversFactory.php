<?php

use Faker\Generator as Faker;

$factory->define(\App\paddock\Drivers\Models\Drivers::class, function (Faker $faker) {
    return [
        'country_id' => function () {
            return factory(\App\paddock\Countries\Models\Countries::class)->create()->code;
        },
        'status' => $faker->randomElement([1, 2]),
        'name' => $faker->name,
        'slug' => $faker->slug,
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'hashtag' => $faker->text,
        'dateofbirth' => $faker->date('Y-m-d'),
        'dateofdeath' => $faker->date('Y-m-d'),
    ];
});
