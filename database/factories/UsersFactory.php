<?php

use Faker\Generator as Faker;

$factory->define(\App\paddock\Users\Models\Users::class, function (Faker $faker) {
    return [
        'email' => $faker->unique()->safeEmail,
        'password' => bcrypt('password'),
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'display_name' => $faker->name,
        'user_name' => $faker->slug,
        'lang' => $faker->languageCode,
        'timezone' => $faker->timezone,
        'avatar' => $faker->imageUrl(),
        'background_image' => '', //TODO: Gallery is still missing
        'remember_token' => str_random(10),
    ];
});
