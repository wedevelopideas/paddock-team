<?php

use Illuminate\Support\Facades\Route;

Route::group(['namespace' => 'Backend', 'prefix' => 'backend', 'middleware' => 'auth'], function () {
    Route::get('', ['uses' => 'HomeController@index', 'as' => 'backend']);

    Route::group(['prefix' => 'drivers'], function () {
        Route::get('', ['uses' => 'DriversController@index', 'as' => 'backend.drivers']);
        Route::get('create', ['uses' => 'DriversController@create', 'as' => 'backend.drivers.create']);
        Route::post('create', ['uses' => 'DriversController@store']);
        Route::get('edit/{id}', ['uses' => 'DriversController@edit', 'as' => 'backend.drivers.edit']);
        Route::post('edit/{id}', ['uses' => 'DriversController@update']);
    });

    Route::group(['prefix' => 'countries'], function () {
        Route::get('', ['uses' => 'CountriesController@index', 'as' => 'backend.countries']);
        Route::get('create', ['uses' => 'CountriesController@create', 'as' => 'backend.countries.create']);
        Route::post('create', ['uses' => 'CountriesController@store']);
        Route::get('edit/{code}', ['uses' => 'CountriesController@edit', 'as' => 'backend.countries.edit']);
        Route::post('edit/{code}', ['uses' => 'CountriesController@update']);
    });

    Route::group(['prefix' => 'grandprixs'], function () {
        Route::get('', ['uses' => 'GrandPrixsController@index', 'as' => 'backend.grandprixs']);
        Route::get('create', ['uses' => 'GrandPrixsController@create', 'as' => 'backend.grandprixs.create']);
        Route::post('create', ['uses' => 'GrandPrixsController@store']);
        Route::get('edit/{id}', ['uses' => 'GrandPrixsController@edit', 'as' => 'backend.grandprixs.edit']);
        Route::post('edit/{id}', ['uses' => 'GrandPrixsController@update']);
    });

    Route::group(['prefix' => 'seasons'], function () {
        Route::get('', ['uses' => 'SeasonsController@index', 'as' => 'backend.seasons']);
        Route::get('create', ['uses' => 'SeasonsController@create', 'as' => 'backend.seasons.create']);
        Route::post('create', ['uses' => 'SeasonsController@store']);
        Route::get('edit/{season}', ['uses' => 'SeasonsController@edit', 'as' => 'backend.seasons.edit']);
        Route::post('edit/{season}', ['uses' => 'SeasonsController@update']);

        Route::group(['prefix' => '{season}'], function () {
            Route::get('races', ['uses' => 'SeasonsRacesController@races', 'as' => 'backend.seasons.races']);
        });
    });

    Route::group(['prefix' => 'tracks'], function () {
        Route::get('', ['uses' => 'TracksController@index', 'as' => 'backend.tracks']);
        Route::get('create', ['uses' => 'TracksController@create', 'as' => 'backend.tracks.create']);
        Route::post('create', ['uses' => 'TracksController@store']);
        Route::get('edit/{id}', ['uses' => 'TracksController@edit', 'as' => 'backend.tracks.edit']);
        Route::post('edit/{id}', ['uses' => 'TracksController@update']);
    });

    Route::group(['prefix' => 'users'], function () {
        Route::get('', ['uses' => 'UsersController@index', 'as' => 'backend.users']);
    });
});

Route::group(['namespace' => 'Frontend'], function () {
    Route::group(['namespace' => 'Auth'], function () {
        Route::get('', ['uses' => 'LoginController@showLoginForm', 'as' => 'index']);
        Route::get('login', ['uses' => 'LoginController@showLoginForm', 'as' => 'login']);
        Route::post('login', ['uses' => 'LoginController@handleLogin']);
        Route::get('logout', ['uses' => 'LoginController@handleLogout', 'as' => 'logout']);
    });

    Route::group(['middleware' => 'auth'], function () {
        Route::get('dashboard', ['uses' => 'DashboardController@index', 'as' => 'dashboard']);

        Route::group(['prefix' => 'settings'], function () {
            Route::get('', ['uses' => 'SettingsController@index', 'as' => 'settings']);
            Route::post('', ['uses' => 'SettingsController@updateUser', 'as' => 'settings.updateUser']);
        });
    });
});
